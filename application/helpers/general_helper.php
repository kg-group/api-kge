<?php

/**
 * use this to print link location
 *
 * @param string $uri
 * @return print url
 */
if (!function_exists('echo_uri')) {

  function echo_uri($uri = "") {
    echo get_uri($uri);
  }

}

/**
 * prepare uri
 * 
 * @param string $uri
 * @return full url 
 */
if (!function_exists('get_uri')) {

  function get_uri($uri = "") {
    $ci = get_instance();
    $index_page = $ci->config->item('index_page');
    return base_url($index_page . '/' . $uri);
  }

}

/**
 * use this to print file path
 * 
 * @param string $uri
 * @return full url of the given file path
 */
if (!function_exists('get_file_uri')) {

  function get_file_uri($uri = "") {
    return base_url($uri);
  }

}

/**
 * check the array key and return the value 
 * 
 * @param array $array
 * @return extract array value safely
 */
if (!function_exists('get_array_value')) {

  function get_array_value(array $array, $key) {
    if (array_key_exists($key, $array)) {
      return $array[$key];
    }
  }

}

/**
 * get the defined config value by a key
 * @param string $key
 * @return config value
 */
if (!function_exists('get_setting')) {

  function get_setting($key = "") {
      $ci = get_instance();
      return $ci->config->item($key);
  }

}


if (!function_exists("dd")) {

  function dd($data = array()) {
    echo "<PRE>";
    print_r($data);
    echo "</PRE>";
  }

}


if (!function_exists("get_driver_info")) {

  function get_driver_info($id = '') {
    $ci = get_instance();

    $model_info = $ci->App_driver_model->get_one($id);

    $data = array(
      'driverId' => $model_info->id,
      'firstname' => $model_info->firstname,
      'lastname' => $model_info->lastname,
      'mobile' => $model_info->mobile,
      'pathImage' => $model_info->path_image,
      'statusInOut' => $model_info->status_in_out,
      'dateTime' => '',
      'truckStatus' => false,
      'plateId' => 'ยังไม่ได้ระบุทะเบียน',
      'autoType' => '-',
      'autoStatusType' => '-',
      'dateTimeIn' => '',
      'dateTimeOut' => '',
      'statusTruck' => '-',
      'Hr' => 0,
      'Mi' => 0,
      'Se' => 0,
    );

    $options = array(
      "type" => "checkInOut",
      "driver_id" => $data["driverId"],
      "status" => "IN"
    );
    $get_first_check_in = $ci->Logs_activities_model->get_first_check_in($options)->result();
    if(count($get_first_check_in) > 0){
      $data["dateTimeIn"] = $get_first_check_in[0]->created_at;
    }

    $options = array(
      "type" => "checkInOut",
      "driver_id" => $data["driverId"],
      "status" => "OUT"
    );
    $get_last_check_out = $ci->Logs_activities_model->get_last_check_out($options)->result();
    if(count($get_last_check_out) > 0){
      $data["dateTimeOut"] = $get_last_check_out[0]->created_at;
    }

    $options = [
      'DRIVER_ID' => $data["driverId"]
    ];
    $truck_info = $ci->Sys_truck_model->get_one_where($options);
    if($truck_info->id){

      $data['message'] = 'บันทึกรถทะเบียน '.$truck_info->PLATE_ID.' เรียบร้อย';
      $data['truckStatus'] = true;
      $data['plateId'] = $truck_info->PLATE_ID;
      $data['autoType'] = $truck_info->AUTO_TYPE;

      if($truck_info->BREAK_DOWN == 'Y'){
        $data['autoStatusType'] = 'อยู่ระหว่างซ่อมบำรุง';
        $data['statusTruck'] = '** อยู่ระหว่างซ่อมบำรุง \n** กรุณาติดต่อเจ้าหน้าที่';
      }
      else if($truck_info->BACKUP == 'Y'){
        $data['autoStatusType'] = 'รถสำรอง';
      }
      else{
        $data['autoStatusType'] = 'พร้อมใช้งาน';
      }
    }

    $diff = 0;
    if($data['dateTimeIn'] != '' and $data['dateTimeOut'] != ''){
      if($data['dateTimeOut'] > $data['dateTimeIn']){
        $diff += abs(strtotime($data['dateTimeIn']) - strtotime($data['dateTimeOut']));
      }
    }

    if($diff > 0){
      $cal_time = cal_time($diff);
      
      $data['Hr'] = $cal_time['Hr'];
      $data['Mi'] = $cal_time['Mi'];
      $data['Se'] = $cal_time['Se'];

    }

    return $data;
  }

}


if (!function_exists("cal_time")) {
  function cal_time($diff){
	  // $diff = abs(strtotime($d1) - strtotime($d2));

	  $s = $diff;
	  $m = $diff/60;
	  $h = $diff/3600;

	  $exm = explode('.', $h);
	  $exs = explode('.', $m);

	  $m = '.'.$exm[1];
	  $s = '.'.$exs[1];

	  $min = $m*60;
	  $sec = $s*60;
	   
	  $data = array(
		  "Hr" => $exm[0],
			"Mi" => floor($min),
			"Se" => round($sec,0)
	  );
	   
	  return $data;
  }
}