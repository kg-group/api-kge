<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Restclient.php';

class Users extends REST_Controller {

	public function __construct(){
    parent::__construct();
    
  }
  

  public function index_post(){
		$dataPost = json_decode($this->input->raw_input_stream, true);
		
		$main_menu = $dataPost["mainMenu"];

		switch ($main_menu) {
				case "Login":
							$username = $dataPost["username"];
							$password = $dataPost["password"];

							$results = $this->login($username, $password);

							$code = REST_Controller::HTTP_NOT_FOUND;
							if(count($results) > 0){
								$code = REST_Controller::HTTP_OK;
							}

							$response = [
								"status" => array(
									"code" => $code,
									"message" => ""
								),
								"result" => $results,
							];
							$this->set_response($response, REST_Controller::HTTP_OK);

						break;
				case "checkPassEmployee":
							$results = $this->check_pass_employee($dataPost);

							$code = REST_Controller::HTTP_NOT_FOUND;
							if(count($results) > 0){
								$code = REST_Controller::HTTP_OK;
							}

							$response = [
								"status" => array(
									"code" => $code,
									"message" => ""
								),
								"result" => $results,
							];
							$this->set_response($response, REST_Controller::HTTP_OK);

						break;
				case "checkInOut":
							$results = $this->check_in_out($dataPost);

							$code = REST_Controller::HTTP_NOT_FOUND;
							if(count($results) > 0){
								$code = REST_Controller::HTTP_OK;
							}

							$response = [
								"status" => array(
									"code" => $code,
									"message" => ""
								),
								"result" => $results,
							];
							$this->set_response($response, REST_Controller::HTTP_OK);
							
						break;
				case "updateLatLon":
							$insert = array(
								"type" => "updateLatLon",
								"lat" => $dataPost["lat"],
								"lon" => $dataPost["lon"],
								"users_id" => $dataPost["driverId"],
							);
							$results = $this->Logs_activities_model->save($insert);

							$response = [
								"status" => array(
									"code" => REST_Controller::HTTP_OK,
									"message" => ""
								),
								"result" => array(),
							];
							$this->set_response($response, REST_Controller::HTTP_OK);
						break;
				default:
						echo "Your favorite color is neither red, blue, nor green!";
		}


	}
	
	public function login($username = "", $password = ""){
		$data = array(); 

		$options = array(
			"username" => $username,
			"password" => $password,
		);
		$model_info = $this->Users_model->get_details($options)->result();

		if(count($model_info) > 0){
			$data = array(
				"driverId" => $model_info[0]->id,
				"firstname" => $model_info[0]->firstname,
				"lastname" => $model_info[0]->lastname,
				"mobile" => $model_info[0]->mobile,
				"pathImage" => $model_info[0]->path_image,
				"statusInOut" => $model_info[0]->status_in_out,
				"dateTime" => "",
				"truckStatus" => false,
				"plateId" => "ยังไม่ได้ระบุทะเบียน",
				"autoType" => "-",
				"autoStatusType" => "-",
				"dateTimeIn" => "",
				"dateTimeOut" => "",
				"statusTruck" => "-",
				"Hr" => 0,
				"Mi" => 0,
				"Se" => 0,
			);

			$options = array(
				"type" => "checkInOut",
				"driver_id" => $data["driverId"],
				"status" => "IN"
			);
			$get_first_check_in = $this->Logs_activities_model->get_first_check_in($options)->result();
			if(count($get_first_check_in) > 0){
				$data["dateTimeIn"] = $get_first_check_in[0]->created_at;
			}

			$options = array(
				"type" => "checkInOut",
				"driver_id" => $data["driverId"],
				"status" => "OUT"
			);
			$get_last_check_out = $this->Logs_activities_model->get_last_check_out($options)->result();
			if(count($get_last_check_out) > 0){
				$data["dateTimeOut"] = $get_last_check_out[0]->created_at;
			}

		}
		

		return $data;
	}

	public function check_pass_employee($data = array(), $type = "EMPLOYEE"){
		$result = array(); 

		$options = array(
			"password" => $data["password"],
			"type" => $type,
		);
		$model_info = $this->Users_model->get_details($options)->result();

		if(count($model_info) > 0){
			$result = array(
				"employeeId" => $model_info[0]->id
			);
		}

		return $result;
	}

	public function check_in_out($data = array()){
		$date_time = date("Y-m-d H:i:s");
		$result = array(
			"dateTime" => $date_time,
			"dateTimeIn" => "",
			"dateTimeOut" => "",
		); 

		if($data["status"] == "IN"){
			$update = array(
				"status_in_out" => 1
			);
		}
		else if($data["status"] == "OUT"){
			$update = array(
				"status_in_out" => 0
			);
		}
		else{
			
		}
		
		$rs_update = $this->Users_model->save($update, $data["driverId"]);

		if($rs_update){
			$insert = array(
				"type" => "checkInOut",
				"status" => $data["status"],
				"users_employee_id" => $data["employeeId"],
				"users_id" => $data["driverId"],
				"created_at" => $date_time,
			);

			$rs_insert = $this->Logs_activities_model->save($insert);


			$options = array(
				"type" => "checkInOut",
				"driver_id" => $data["driverId"],
				"status" => "IN"
			);
			$get_first_check_in = $this->Logs_activities_model->get_first_check_in($options)->result();
			if(count($get_first_check_in) > 0){
				$result["dateTimeIn"] = $get_first_check_in[0]->created_at;
			}

			$options = array(
				"type" => "checkInOut",
				"driver_id" => $data["driverId"],
				"status" => "OUT"
			);
			$get_last_check_out = $this->Logs_activities_model->get_last_check_out($options)->result();
			if(count($get_last_check_out) > 0){
				$result["dateTimeOut"] = $get_last_check_out[0]->created_at;
			}


		}





		return $result;
	}








  public function save(){
    
  }

  public function update(){

    $unid_number = 0;
		$sql = "SELECT IFNULL(COUNT(UNID),0) as unid_number FROM stk_or ";
		$rs = $this->db->query($sql)->result();
		if(count($rs) > 0){
			$unid_number = $rs[0]->unid_number;
    }
			
		$data = array(
			"mainMenu" => "getStkOr",
			"start_row" => $unid_number,
			"limit_row" => 500
    );
    
		$param = json_encode($data);
			
		$post = $this->rest_cli->post($this->uri_kgcall, $param, $this->header_kgcall);				
		
		$response = json_decode($post->response);

		if(count($response->result->data) > 0){
			foreach($response->result->data as $key => $value){
				$insert = (array) $value;
				
				$this->db->insert("stk_or", $insert);
				
			}
		}

  }

  public function update_stk_or(){

    $unid_number = 0;
		$sql = "SELECT IFNULL(COUNT(UNID),0) as unid_number FROM stk_or ";
		$rs = $this->db->query($sql)->result();
		if(count($rs) > 0){
			$unid_number = $rs[0]->unid_number;
    }
			
		$data = array(
			"mainMenu" => "getStkOr",
			"start_row" => $unid_number,
			"limit_row" => 2000
    );
    
		$param = json_encode($data);
			
		$post = $this->rest_cli->post($this->uri_kgcall, $param, $this->header_kgcall);				
		
		$response = json_decode($post->response);

		if(count($response->result->data) > 0){
			foreach($response->result->data as $key => $value){
				$insert = (array) $value;
				
				$this->db->insert("stk_or", $insert);
				
			}
		}

  }

  public function update_stk_pos2(){

    $unid_number = 0;
		$sql = "SELECT IFNULL(COUNT(UNID),0) as unid_number FROM stk_pos2 ";
		$rs = $this->db->query($sql)->result();
		if(count($rs) > 0){
			$unid_number = $rs[0]->unid_number;
    }
			
		$data = array(
			"mainMenu" => "getStkPos2",
			"start_row" => $unid_number,
			"limit_row" => 2000
    );
    
		$param = json_encode($data);
			
		$post = $this->rest->post($this->uri_kgcall, $param, $this->header_kgcall);				
		
		$response = json_decode($post->response);

		if(count($response->result->data) > 0){
			foreach($response->result->data as $key => $value){
				$insert = (array) $value;
				
				$this->db->insert("stk_pos2", $insert);
				
			}
		}

  }
  

}
