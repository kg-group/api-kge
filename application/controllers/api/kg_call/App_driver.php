<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class App_driver extends REST_Controller {

  protected $settings;
  protected $url;
  protected $header = [];
  protected $start_row = 0;
  protected $limit_row = 2000;

	public function __construct(){
    parent::__construct();

    $this->load->library("restclient");
    $this->rest_cli = new RestClient();

    $this->settings = $this->config->item('kgt');
    $this->url = $this->settings['call']['settings']['url'];
    $this->header = [
			'x-access-token' => $this->settings['call']['settings']['x-access-token']
    ];
    
  }
  
  public function index_get(){

    /**------ เริ่มอัพเดทข้อมูลใหม่ ------**/
    $data = $this->new_data();
    if(count($data) > 0){
      foreach($data as $key => $value){
        $insert = (array) $value;
        unset($insert['id']);

        $save_id = $this->App_driver_model->save($insert);
        if(!$save_id){
          echo $value->id . 'Save error!!!. <br>';
        }
        else{
          echo $value->id . 'Save success!!!. <br>';
        }
      }
    }
    /**------ จบอัพเดทข้อมูลใหม่ ------**/

  }

  public function update_get(){
    echo 'FUNCTION IS NULL!!';
  }

  public function new_data(){
    $get_limit = $this->App_driver_model->get_limit_number()->result();
    if(count($get_limit) > 0)$this->start_row = $get_limit[0]->unid_number;

    $data = [
			'mainMenu' => 'getAppDriver',
			'start_row' => $this->start_row,
			'limit_row' => $this->limit_row
    ];
    $param = json_encode($data);
      
    $post = $this->rest_cli->post($this->url, $param, $this->header);			
		$response = json_decode($post->response);

    return $response->result->data;
  }

}
