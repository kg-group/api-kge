<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Stk_pos2 extends REST_Controller {

  protected $settings;
  protected $url;
  protected $header = [];
  protected $start_row = 0;
  protected $limit_row = 2000;

	public function __construct(){
    parent::__construct();
    
    $this->load->library("restclient");
    $this->rest_cli = new RestClient();

    $this->settings = $this->config->item('kgt');
    $this->url = $this->settings['call']['settings']['url'];
    $this->header = [
			'x-access-token' => $this->settings['call']['settings']['x-access-token']
    ];
    
  }
  
  public function index_get(){

    /**------ เริ่มอัพเดทข้อมูลใหม่ ------**/
    $data = $this->new_data();
    if(count($data) > 0){
      foreach($data as $key => $value){
        $insert = (array) $value;

        $save_id = $this->Stk_pos2_model->save($insert);
        if(!$save_id){
          echo $insert['UNID'] . 'Save error!!!. <br>';
        }
        else{
          echo $insert['UNID'] . 'Save success!!!. <br>';

          $url_fcm = "https://fcm.googleapis.com/fcm/send";
          $data = [
            'to' => 'doXR2URj94Y:APA91bEmglw8SxrSwVV5rgTqoc3H7wIUD8hU22T7WK6DFjCHWFf31i_LAiTRojk2xgcqQP6akEjBwNzHWDQBw4cQu590ZnI0huzfeOA6kv9RBHNOVmgVbVBzade1H2X7swnfiQNxEXF8',
            'notification' => [
              'body' => 'great match!',
              'title' => 'Portugal vs. Denmark',
              'icon' => 'myicon'
            ]
          ];
          $param = json_encode($data);

          $header_fcm = [
            'Content-Type' => 'application/json',
            'Authorization' => 'key=AAAAei3DuXQ:APA91bEZAgzkZ_eJVKDpNU3pCwHR_PbffMLE_b33ghOomOabv0OJuXlD-H5JvDavsq6mKSVMGjhlzWxfmSPibGTkkCPEtsrEI-QevAA1rfZdGFhH_kyJGC0nPrLbcjyTJZRffG9pODi_'
          ];
            
          // $post = $this->rest_cli->post($url_fcm, $param, $header_fcm);			

        }
      }
    }
    /**------ จบอัพเดทข้อมูลใหม่ ------**/

  }

  public function update_get(){

    /**------ เริ่มอัพเดทข้อมูลเก่า ------**/
    $data = $this->old_data();
    if(count($data) > 0){
      foreach($data as $key => $value){
        $get_id = $this->Stk_pos2_model->get_one_where(['UNID' => $value->UNID]);
        if($get_id->id != ''){
          $update = (array) $value;
          $this->Stk_pos2_model->save($update, $get_id->id);
        }
      }
    }
    /**------ จบอัพเดทข้อมูลเก่า ------**/

  }

  public function new_data(){
    $get_limit = $this->Stk_pos2_model->get_limit_number()->result();
    if(count($get_limit) > 0)$this->start_row = $get_limit[0]->unid_number;

    $data = [
			'mainMenu' => 'getStkPos2',
			'start_row' => $this->start_row,
			'limit_row' => $this->limit_row
    ];
    $param = json_encode($data);
      
    $post = $this->rest_cli->post($this->url, $param, $this->header);			
		$response = json_decode($post->response);

    return $response->result->data;
  }

  public function old_data(){
    $start_date = date('Y-m-d');
    $end_date = date('Y-m-d');
    $data = [
      'mainMenu' => 'getStkPos2Date',
      'start_date' => $start_date,
      'end_date' => $end_date,
			'start_row' => $this->start_row,
			'limit_row' => $this->limit_row
    ];
    $param = json_encode($data);
      
    $post = $this->rest_cli->post($this->url, $param, $this->header);			
		$response = json_decode($post->response);

    return $response->result->data;
  }
  

}
