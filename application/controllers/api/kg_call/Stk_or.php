<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Stk_or extends REST_Controller {

  protected $settings;
  protected $url;
  protected $header = [];
  protected $start_row = 0;
  protected $limit_row = 2000;

	public function __construct(){
    parent::__construct();

    $this->load->library("restclient");
    $this->rest_cli = new RestClient();

    $this->settings = $this->config->item('kgt');
    $this->url = $this->settings['call']['settings']['url'];
    $this->header = [
			'x-access-token' => $this->settings['call']['settings']['x-access-token']
    ];
    
  }
  
  public function index_get(){

    /**------ เริ่มอัพเดทข้อมูลใหม่ ------**/
    $data = $this->new_data();
    if(count($data) > 0){
      foreach($data as $key => $value){
        $insert = (array) $value;

        $save_id = $this->Stk_or_model->save($insert);
        if(!$save_id){
          echo $insert['UNID'] . 'Save error!!!. <br>';
        }
        else{
          echo $insert['UNID'] . 'Save success!!!. <br>';
        }
      }
    }
    /**------ จบอัพเดทข้อมูลใหม่ ------**/

  }

  public function update_get(){

    /**------ เริ่มอัพเดทข้อมูลเก่า ------**/
    $data = $this->old_data();
    if(count($data) > 0){
      foreach($data as $key => $value){
        $get_id = $this->Stk_or_model->get_one_where(['UNID' => $value->UNID]);
        if($get_id->id != ''){
          $update = (array) $value;
          $this->Stk_or_model->save($update, $get_id->id);
        }
      }
    }
    /**------ จบอัพเดทข้อมูลเก่า ------**/

  }

  public function new_data(){
    $get_limit = $this->Stk_or_model->get_limit_number()->result();
    if(count($get_limit) > 0)$this->start_row = $get_limit[0]->unid_number;

    $data = [
			'mainMenu' => 'getStkOr',
			'start_row' => $this->start_row,
			'limit_row' => $this->limit_row
    ];
    $param = json_encode($data);
      
    $post = $this->rest_cli->post($this->url, $param, $this->header);			
		$response = json_decode($post->response);

    return $response->result->data;
  }

  public function old_data(){
    $start_date = date('Y-m-d');
    $end_date = date('Y-m-d');
    $data = [
      'mainMenu' => 'getStkOrDate',
      'start_date' => $start_date,
      'end_date' => $end_date,
			'start_row' => $this->start_row,
			'limit_row' => $this->limit_row
    ];
    $param = json_encode($data);
      
    $post = $this->rest_cli->post($this->url, $param, $this->header);			
		$response = json_decode($post->response);

    return $response->result->data;
  }
  

}
