<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Leave extends REST_Controller {

	public function __construct(){
    parent::__construct();

  }
  
  public function index_post(){
    $param = json_decode($this->input->raw_input_stream, true);
    
    // dd($param);exit;

    if(count($param['arrOrders']) > 0 and $param['driverId']){
      foreach($param['arrOrders'] as $key => $value){
        $data = [
          'driver_id' => $param['driverId'],
          'APP_TIME_OUT' => date('Y-m-d H:i:s')
        ];
        $where = [
          'BILL_ID' => $value['BILL_ID']
        ];
        $this->Stk_pos2_model->update_where($data, $where);
      }
    }

    $response = [
      'status' => array(
        'code' => REST_Controller::HTTP_OK,
        'message' => $this->http_status_codes[REST_Controller::HTTP_OK]
      ),
      'result' => [
        'code' => REST_Controller::HTTP_OK
      ]
    ];
    $this->set_response($response, REST_Controller::HTTP_OK);


  }

  public function receive_post(){
    $param = json_decode($this->input->raw_input_stream, true);

    if($param['driverId'] and $param['arrOrders']['BILL_ID']){

      $date_time_now = date('Y-m-d H:i:s');

      $data = [
        'END_DATE' =>  $date_time_now,
        'APP_TIME_RECEIVE' =>  $date_time_now
      ];
      $where = [
        'BILL_ID' => $param['arrOrders']['BILL_ID']
      ];
      $rs = $this->Stk_pos2_model->update_where($data, $where);

      if(!$rs){
        $response = [
          'status' => array(
            'code' => REST_Controller::HTTP_FORBIDDEN,
            'message' => $this->http_status_codes[REST_Controller::HTTP_FORBIDDEN]
          ),
          'result' => [],
        ];
        $this->set_response($response, REST_Controller::HTTP_OK);
      }
      else{

        
        $receive_file_path = get_setting("receive_file_path");
        $target_path = getcwd() . '/' . $receive_file_path . date('Y') . '/' . date('m');
        if (!is_dir($target_path)) {
          if (!mkdir($target_path, 0777, true)) {
            die('Failed to create file folders.');
          }
        }
        
        $img = $param["avatarBase64"];
				list($type, $img) = explode(';', $img);
        list(, $img)      = explode(',', $img);
        $image_save = 'Thumbnails_'.$param['arrOrders']['BILL_ID'].'.jpg';
        $decoded = base64_decode($img);
        $status_file = file_put_contents($target_path . '/' . $image_save,$decoded);
        if($status_file){
          $update = [
            'APP_PATH_IMAGE' => $image_save
          ];
          $where = [
            'BILL_ID' => $param['arrOrders']['BILL_ID']
          ];
          $this->Stk_pos2_model->update_where($update, $where);
        }

        $response = [
          'status' => array(
            'code' => REST_Controller::HTTP_OK,
            'message' => $this->http_status_codes[REST_Controller::HTTP_OK]
          ),
          'result' => [
            'code' => REST_Controller::HTTP_OK
          ]
        ];
        $this->set_response($response, REST_Controller::HTTP_OK);
      }

    }

    // dd($param);

  }


  
}
