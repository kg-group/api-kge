<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Signin extends REST_Controller {

	public function __construct(){
    parent::__construct();
    
  }
  
  public function index_post(){
    $param = json_decode($this->input->raw_input_stream, true);
    
    $auth = $this->authenticate($param['username'], $param['password']);

    if(!$auth){
      $response = [
        'status' => array(
          'code' => REST_Controller::HTTP_FORBIDDEN,
          'message' => $this->http_status_codes[REST_Controller::HTTP_FORBIDDEN]
        ),
        'result' => [],
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);
    }
    else{

      $model_info = $this->App_driver_model->get_one_where(['username' => $param['username'], 'password' => $param['password']]);

      $result = get_driver_info($model_info->id); /** ดึงข้อมูล พนง. helper -> general **/
      
      $response = [
        'status' => array(
          'code' => REST_Controller::HTTP_OK,
          'message' => $this->http_status_codes[REST_Controller::HTTP_OK]
        ),
        'result' => $result,
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);

    }

  }

  function authenticate($username = '', $password = ''){

    if (!$this->App_driver_model->authenticate($username, $password)){
      return false;
    }

    return true;
  }

  function signout_post(){
    echo 1;
  }


}
