<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Truck extends REST_Controller {

	public function __construct(){
    parent::__construct();

  }
  
  public function index_post(){
    $param = json_decode($this->input->raw_input_stream, true);
    
    $options = ['PLATE_ID' => $param['truckNo']];
    $model_info = $this->Sys_truck_model->get_one_where($options);
    
    if($model_info->DRIVER_ID and $model_info->DRIVER_ID != $param['driverId']){
      $result = get_driver_info($model_info->DRIVER_ID);

      $response = [
        'status' => array(
          'code' => REST_Controller::HTTP_FORBIDDEN,
          'message' => $this->http_status_codes[REST_Controller::HTTP_FORBIDDEN]
        ),
        'result' => [
          'message' => 'ชื่อ '.$result['firstname'].' '.$result['lastname'].' \nโทร. '.$result['mobile']
        ]
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);
    }
    else{

      $update = ['DRIVER_ID' => $param['driverId']];
      $this->Sys_truck_model->save($update, $model_info->id);

      $data = [
        'type' => $param['mainMenu'],
        'params' => serialize($param),
        'status' => 'IN',
        'users_id' => $param['driverId'],
        'created_at' => date('Y-m-d H:i:s')
      ];
      $this->Logs_activities_model->save($data);

      $result = get_driver_info($param['driverId']);

      $response = [
        'status' => array(
          'code' => REST_Controller::HTTP_OK,
          'message' => $this->http_status_codes[REST_Controller::HTTP_OK]
        ),
        'result' => $result,
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);

    }

  }


  function change_post(){
    $param = json_decode($this->input->raw_input_stream, true);

    $data = ['DRIVER_ID' => '0'];
    $where = ['PLATE_ID' => $param['truckNo']];
    $update = $this->Sys_truck_model->update_where($data, $where);
    if(!$update){
      $response = [
        'status' => array(
          'code' => REST_Controller::HTTP_FORBIDDEN,
          'message' => $this->http_status_codes[REST_Controller::HTTP_FORBIDDEN]
        ),
        'result' => []
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);
    }
    else{

      $data = [
        'type' => $param['mainMenu'],
        'params' => serialize($param),
        'status' => 'OUT',
        'users_id' => $param['driverId'],
        'created_at' => date('Y-m-d H:i:s')
      ];
      $this->Logs_activities_model->save($data);

      $result = get_driver_info($param['driverId']); /** ดึงข้อมูล พนง. helper -> general **/
      $response = [
        'status' => array(
          'code' => REST_Controller::HTTP_OK,
          'message' => $this->http_status_codes[REST_Controller::HTTP_OK]
        ),
        'result' => $result,
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);

    }

  }

  
}
