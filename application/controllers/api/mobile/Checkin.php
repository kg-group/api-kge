<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Checkin extends REST_Controller {

	public function __construct(){
    parent::__construct();

  }
  
  public function index_post(){
    $param = json_decode($this->input->raw_input_stream, true);
    
    if(!$param['driverId'] or (!$param['employeeId'] and $param['status'] == 'IN')){
      $response = [
        'status' => array(
          'code' => REST_Controller::HTTP_FORBIDDEN,
          'message' => $this->http_status_codes[REST_Controller::HTTP_FORBIDDEN]
        ),
        'result' => []
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);
    }
    else{

      $arr_status = [
        'IN' => 1,
        'OUT' => 0
      ];

      $update = [
        'status_in_out' => $arr_status[$param['status']]
      ];

      $this->App_driver_model->save($update, $param['driverId']);

      $data = [
        'type' => $param['mainMenu'],
        'status' => $param['status'],
        'users_employee_id' => $param['employeeId'],
        'users_id' => $param['driverId'],
        'created_at' => date('Y-m-d H:i:s')
      ];

      $save_id = $this->Logs_activities_model->save($data);
      if(!$save_id){
        $response = [
          'status' => array(
            'code' => REST_Controller::HTTP_FORBIDDEN,
            'message' => $this->http_status_codes[REST_Controller::HTTP_FORBIDDEN]
          ),
          'result' => []
        ];
        $this->set_response($response, REST_Controller::HTTP_OK);
      }
      else{
        $result = get_driver_info($param['driverId']); /** ดึงข้อมูล พนง. helper -> general **/
        $response = [
          'status' => array(
            'code' => REST_Controller::HTTP_OK,
            'message' => $this->http_status_codes[REST_Controller::HTTP_OK]
          ),
          'result' => $result,
        ];
        $this->set_response($response, REST_Controller::HTTP_OK);
      }

    }

  }

  public function auth_post(){
    $param = json_decode($this->input->raw_input_stream, true);

    $auth = $this->App_driver_model->get_one_where(['password' => $param['password']]);
    
    if($auth->id == ''){
      $response = [
        'status' => array(
          'code' => REST_Controller::HTTP_FORBIDDEN,
          'message' => $this->http_status_codes[REST_Controller::HTTP_FORBIDDEN]
        ),
        'result' => []
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);
    }
    else{
      $response = [
        'status' => array(
          'code' => REST_Controller::HTTP_OK,
          'message' => $this->http_status_codes[REST_Controller::HTTP_OK]
        ),
        'result' => [
          'employeeId' => $auth->id,
          'infoModel' => $auth
        ]
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);
    }

  }

  

}
