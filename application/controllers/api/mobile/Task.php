<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Task extends REST_Controller {

	public function __construct(){
    parent::__construct();

  }
  
  public function index_post(){
    $param = json_decode($this->input->raw_input_stream, true);
    
    // dd($param);

    $result = [];

    $options = ['truckNo' => $param['truckNo']];
    $ticket_info = $this->Stk_pos2_model->get_details($options)->result();
    if(count($ticket_info) > 0){
      foreach($ticket_info as $key => $value){
        $result[] = $value;
      }

      // dd($ticket_info);

      $response = [
        'status' => array(
          'code' => REST_Controller::HTTP_OK,
          'message' => $this->http_status_codes[REST_Controller::HTTP_OK]
        ),
        'result' => $result,
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);


    }

  }
  
}
