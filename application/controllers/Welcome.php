<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->library("restclient");
		$this->rest = new RestClient();
	}

	public function index(){
		// $sql = "SELECT * FROM users ";
		// $rs = $this->db->query($sql)->result();
		// print_r($rs);
		// $this->load->view('welcome_message');

		// $insert = array(
		// 	"value" => "test"
		// );
		// $this->db->insert("test", $insert);
		echo 1;
		exit;

		$unid_number = 0;
		$sql = "SELECT IFNULL(COUNT(UNID),0) as unid_number FROM stk_or ";
		$rs = $this->db->query($sql)->result();
		if(count($rs) > 0){
			$unid_number = $rs[0]->unid_number;
		}

		// echo $filter_date;
		// exit;

		$url = "http://logistics.kgt.co.th/app/driver/index.php";
			
		$data = array(
			"mainMenu" => "getStkOr",
			"start_date" => "2019-09-26 00:00:00",
			"end_date" => "2019-09-26 23:59:59",
			"start_row" => $unid_number,
			"limit_row" => 2000
		);

		$param = json_encode($data);
			
		$header = array(
			"x-access-token" => "201903271114445c9af8b4016281553660084"
		);
		
		$post = $this->rest->post($url, $param, $header);				
		
		$response = json_decode($post->response);

		if(count($response->result->data) > 0){
			foreach($response->result->data as $key => $value){
				$insert = (array) $value;
				
				$this->db->insert("stk_or", $insert);
				
			}
		}







		$unid_number = 0;
		$sql = "SELECT IFNULL(COUNT(UNID),0) as unid_number FROM stk_pos2 ";
		$rs = $this->db->query($sql)->result();
		if(count($rs) > 0){
			$unid_number = $rs[0]->unid_number;
		}

		// echo $filter_date;
		// exit;

		$url = "http://logistics.kgt.co.th/app/driver/index.php";
			
		$data = array(
			"mainMenu" => "getStkPos2",
			"start_date" => "2019-09-26 00:00:00",
			"end_date" => "2019-09-26 23:59:59",
			"start_row" => $unid_number,
			"limit_row" => 2000
		);

		$param = json_encode($data);
			
		$header = array(
			"x-access-token" => "201903271114445c9af8b4016281553660084"
		);
		
		$post = $this->rest->post($url, $param, $header);				
		
		$response = json_decode($post->response);

		if(count($response->result->data) > 0){
			foreach($response->result->data as $key => $value){
				$insert = (array) $value;
				
				$this->db->insert("stk_pos2", $insert);
				
			}
		}
		
	}
}
