<?php

class Users_model extends Crud_model {

  private $table = null;

  function __construct() {
    $this->table = 'users';
    parent::__construct($this->table);
  }

  function get_details($options = array()) {
    $users_table = $this->db->dbprefix($this->table);
    
    $where= "";
    $id = get_array_value($options, "id");
    $username = get_array_value($options, "username");
    $password = get_array_value($options, "password");
    $type = get_array_value($options, "type");

    if($id)$where .= " AND $users_table.id = $id";
    if($username)$where .= " AND $users_table.username = '$username'";
    if($password)$where .= " AND $users_table.password = '$password'";
    if($type)$where .= " AND $users_table.type = '$type'";
    
    $sql = "SELECT $users_table.*
            FROM $users_table
            WHERE $users_table.deleted=0 $where";

    return $this->db->query($sql);
  }
}
