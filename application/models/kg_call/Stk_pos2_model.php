<?php

class Stk_pos2_model extends Crud_model {

  private $table = null;

  function __construct() {
    $this->table = 'stk_pos2_kg_call';
    parent::__construct($this->table);
  }

  function get_limit_number(){
    $sql = 'SELECT IFNULL(COUNT(UNID),0) as unid_number FROM ' . $this->table;

		return $this->db->query($sql);
  }

  function get_details($options = array()){

    $truck_no = get_array_value($options, "truckNo");

    $sql ="SELECT BILL_ID,ITEM_NAME,CAST(BILL_WEIGHT_KG AS DECIMAL(8,0)) AS BILL_WEIGHT_KG,CAST(BILL_TOTAL_AMT AS DECIMAL(8,2)) AS BILL_TOTAL_AMT";
		$sql .=",AUTO_DRIVER,AUTO_PLATE_ID,COM_ID";
		$sql .=",CONCAT(REPLACE(REPLACE(REPLACE(REPLACE(CONVERT(SO_ISS_QTY,CHAR(5)),'.0000',''),'.000',''),'.00',''),'.0',''),' ',SO_ISS_UNIT) AS QTY,SHIP_CONTACT ";
		$sql .=",UNID, CUST_ID, CUST_NAME, CUST_TYPE, CUST_ADDR1, CUST_ADDR2, CUST_ADDR3, CUST_PHONE, CUST_FAX, CUST_CONTACT, CUST_CONTACT_PHONE, SHIP_ADDR1, REMARK, AUTO_PLATE_ID, AUTO_TYPE, AUTO_DRIVER ";
		$sql .=",ITEM_NAME, ITEM_PRICE, ITEM_PRICE_DESCRIPTION, ITEM_PRICE_TYPE, SO_ISS_QTY, SO_ISS_UNIT, APP_TIME_OUT, APP_TIME_RECEIVE, APP_PATH_IMAGE ";
		$sql .="FROM ".$this->table." ";
		$sql .="WHERE DATEDIFF(BILL_DATE,NOW())=0 AND CANCEL='N' AND IN_TIME IS NOT NULL AND OUT_TIME IS NOT NULL ";
    $sql .="AND (END_DATE IS NULL OR APP_TIME_RECEIVE IS NULL) ";
    
		if($truck_no != "")$sql .="AND AUTO_PLATE_ID='".$truck_no."' ";
		
    $sql .="ORDER BY BILL_DATE,BILL_TIME ASC ";
    
    // echo $sql;exit;

    return $this->db->query($sql);
  }


}
