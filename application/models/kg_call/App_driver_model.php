<?php

class App_driver_model extends Crud_model {

  private $table = null;

  function __construct() {
    $this->table = 'app_driver_kg_call';
    parent::__construct($this->table);
  }

  function get_limit_number(){
    $sql = 'SELECT IFNULL(COUNT(id),0) as unid_number FROM ' . $this->table;

		return $this->db->query($sql);
  }

  function authenticate($username, $password){
    $this->db->select("*");
    $result = $this->db->get_where($this->table, array('username' => $username, 'password' => $password));
    if($result->num_rows() == 1){
      $user_info = $result->row();
      
      return true;
    }
  }

}
