<?php

class Stk_or_model extends Crud_model {

  private $table = null;

  function __construct() {
    $this->table = 'stk_or_kg_call';
    parent::__construct($this->table);
  }

  function get_limit_number(){
    $sql = 'SELECT IFNULL(COUNT(UNID),0) as unid_number FROM ' . $this->table;

		return $this->db->query($sql);
  }


}
