<?php

class Logs_activities_model extends Crud_model {

  private $table = null;

  function __construct() {
    $this->table = 'logs_activities';
    parent::__construct($this->table);
  }

  function get_details($options = array()) {
    $activities_table = $this->db->dbprefix($this->table);
    
    $where= "";
    $id = get_array_value($options, "id");

    if($id)$where .= " AND $activities_table.id = $id";
    
    $sql = "SELECT $activities_table.*
            FROM $activities_table
            WHERE $activities_table.deleted=0 $where";

    return $this->db->query($sql);
  }

  function get_first_check_in($options = array()){
    $activities_table = $this->db->dbprefix($this->table);

    $driver_id = get_array_value($options, "driver_id");
    $type = get_array_value($options, "type");
    $status = get_array_value($options, "status");

    $sql = "SELECT * 
            FROM $activities_table 
            WHERE type = '".$type."' AND users_id = '".$driver_id."' AND status = '".$status."' AND created_at LIKE '".date("Y-m-d")."%' 
            ORDER BY id ASC 
            LIMIT 0,1 ";

    return $this->db->query($sql);
  }

  function get_last_check_out($options = array()){
    $activities_table = $this->db->dbprefix($this->table);

    $driver_id = get_array_value($options, "driver_id");
    $type = get_array_value($options, "type");
    $status = get_array_value($options, "status");

    $sql = "SELECT * 
            FROM $activities_table 
            WHERE type = '".$type."' AND users_id = '".$driver_id."' AND status = '".$status."' AND created_at LIKE '".date("Y-m-d")."%' 
            ORDER BY id DESC 
            LIMIT 0,1 ";

    return $this->db->query($sql);

  } 

}
