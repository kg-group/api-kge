<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



/** api kg call **/
$route['kgcall/users/index'] = 'api/kg_call/users/index';

$route['kgt/call/employee/index'] = 'api/kg_call/app_driver/index'; // ดึงข้อมูลพนักงานจาก KG CALL มา เก็บที่ Middleware
$route['kgt/call/employee/update'] = 'api/kg_call/app_driver/update'; // ดึงข้อมูลพนักงานที่มีการแก้จาก KG CALL มา อัพเดทที่ Middleware
$route['kgt/call/orders/index'] = 'api/kg_call/stk_or/index'; // ดึงข้อมูลบิลสั่งขายจาก KG CALL มา เก็บที่ Middleware
$route['kgt/call/orders/update'] = 'api/kg_call/stk_or/update'; // ดึงข้อมูลบิลสั่งขายที่มีการแก้จาก KG CALL มา อัพเดทที่ Middleware
$route['kgt/call/tickets/index'] = 'api/kg_call/stk_pos2/index'; // ดึงข้อมูลตั๋วส่งสินค้าจาก KG CALL มา เก็บที่ Middleware
$route['kgt/call/tickets/update'] = 'api/kg_call/stk_pos2/update'; // ดึงข้อมูลตั๋วส่งสินค้าที่มีการแก้จาก KG CALL มา อัพเดทที่ Middleware
$route['kgt/call/truck/index'] = 'api/kg_call/sys_truck/index'; // ดึงข้อมูลรถบรรทุกจาก KG CALL มา เก็บที่ Middleware
$route['kgt/call/truck/update'] = 'api/kg_call/sys_truck/update'; // ดึงข้อมูลรถบรรทุกที่มีการแก้จาก KG CALL มา อัพเดทที่ Middleware

$route['api/driver/signin'] = 'api/mobile/signin/index'; // ล็อคอิน
$route['api/driver/checkin'] = 'api/mobile/checkin/index'; // กดเข้างาน และ ออกจากงาน
$route['api/driver/checkin/password'] = 'api/mobile/checkin/auth'; // เช็คพาสเวิร์ดของ พนง. หน้าท่า
$route['api/driver/truck/select'] = 'api/mobile/truck/index'; // เลือกรถบรรทุก
$route['api/driver/truck/change'] = 'api/mobile/truck/change'; // เปลี่ยนรถบรรทุก
$route['api/driver/task'] = 'api/mobile/task/index'; // รายการจัดส่งสินค้าทั้งหมด
$route['api/driver/leave'] = 'api/mobile/leave/index'; // ออกจากท่า
$route['api/driver/leave/receive'] = 'api/mobile/leave/receive'; // ถ่ายรูปส่งสินค้า
$route['api/driver/signin/signout'] = 'api/mobile/signin/signout'; // ล็อคเอ้า
